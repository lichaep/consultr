<?php

namespace App\Imports;

use App\Models\Publisher;
use App\Models\Race;
use App\Models\SuperHero;
use League\Csv\Reader;

class SuperHeroesImporter
{
    const SUPER_HEROES_FILE_PATH = '../csv/superheros.csv';

    const NAME = 1;
    const FULL_NAME = 2;
    const STRENGTH = 3;
    const SPEED = 4;
    const DURABILITY = 5;
    const POWER = 6;
    const COMBAT = 7;
    const RACE = 8;
    const HEIGHT = 9;
    const HEIGHT1 = 10;
    const WEIGHT = 11;
    const WEIGHT1 = 12;
    const EYE_COLOR = 13;
    const HAIR_COLOR = 14;
    const PUBLISHER = 15;

    /**
     * @return SuperHero[]|array
     */
    public function data(): array
    {
        $reader = Reader::createFromPath(self::SUPER_HEROES_FILE_PATH, 'r')->setOffset(1);
        $results = $reader->fetch();
        $superheros = [];

        foreach ($results as $row) {
            $superheros[] = new SuperHero([
                'name' => $row[self::NAME],
                'fullName' => $row[self::FULL_NAME],
                'strength' => $row[self::STRENGTH],
                'speed' => $row[self::SPEED],
                'durability' => $row[self::DURABILITY],
                'power' => $row[self::POWER],
                'combat' => $row[self::COMBAT],
                'race_id' => optional($this->race($row[self::RACE]))->id,
                'heightInch' => $this->checkIsNull($row[self::HEIGHT]),
                'heightCentimeters' => $row[self::HEIGHT1],
                'weightPounds' => $this->checkIsNull($row[self::WEIGHT]),
                'weightKilogramos' => $row[self::WEIGHT1],
                'eyeColor' => $this->checkIsNull($row[self::EYE_COLOR]),
                'hairColor' => $this->checkIsNull($row[self::HAIR_COLOR]),
                'publisher_id' => optional($this->publisher($row[self::PUBLISHER]))->id,
            ]);
        }

        return $superheros;
    }

    private function checkIsNull(string $data): ?string
    {
        if (trim($data) === '-') {
            return null;
        }

        $explode = explode('-', $data);

        logger('data: '.$data.' | trim 0: ' . trim($explode[0]));

        if (trim($explode[0]) === '') {
            return null;
        }

        return $data;
    }

    private function publisher(string $publisherId): ?Publisher
    {
        if (!$publisherId) {
            return null;
        }

        /** @var Publisher $publisher */
        $publisher = Publisher::query()->where('description', '=', $publisherId)->first();

        if (!$publisher) {
            $publisher = new Publisher(['description' => $publisherId]);
            $publisher->save();
        }

        return $publisher;
    }

    private function race(string $raceId): ?Race
    {
        if (!$raceId) {
            return null;
        }

        /** @var Race $race */
        $race = Race::query()->where('description', '=', $raceId)->first();

        if (!$race) {
            $race = new Race(['description' => $raceId]);
            $race->save();
        }

        return $race;
    }
}
