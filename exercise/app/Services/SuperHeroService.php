<?php

namespace App\Services;

use App\Imports\SuperHeroesImporter;

class SuperHeroService
{
    /** @var SuperHeroesImporter */
    private $heroesImporter;

    public function __construct(SuperHeroesImporter $heroesImporter)
    {
        $this->heroesImporter = $heroesImporter;
    }

    public function importFromCsv(): void
    {
        $data = $this->heroesImporter->data();

        foreach($data as $superHero) {
            $superHero->save();
        }
    }
}
