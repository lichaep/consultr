<?php

namespace App\Http\Requests;

use App\Models\Publisher;
use App\Models\Race;

class SuperHeroesListRequest extends BaseRequest
{
    const NAME = 'name';
    const FULL_NAME = 'fullName';
    const STRENGTH = 'strength';
    const SPEED = 'speed';
    const DURABILITY = 'durability';
    const POWER = 'power';
    const COMBAT = 'combat';
    const RACE = 'race';
    const HEIGHT = 'height/0';
    const HEIGHT1 = 'height/1';
    const WEIGHT = 'weight/0';
    const WEIGHT1 = 'weight/1';
    const EYE_COLOR = 'eyeColor';
    const HAIR_COLOR = 'hairColor';
    const PUBLISHER = 'publisher';

    public function request()
    {
        return \request();
    }

    public function name(): ?string
    {
        return $this->request()->query(self::NAME);
    }

    public function fullName(): ?string
    {
        return $this->request()->query(self::FULL_NAME);
    }

    public function strength(): ?string
    {
        return $this->request()->query(self::STRENGTH);
    }

    public function publisher(): ?Publisher
    {
        if (!$publisherId = $this->request()->query(self::PUBLISHER)) {
            return null;
        }

        /** @var Publisher $publisher */
        $publisher = Publisher::query()->where('id','=', $publisherId)->first();

        return $publisher;
    }

    public function race(): ?Race
    {
        if (!$raceId = $this->request()->query(self::RACE)) {
            return null;
        }

        /** @var Race $race */
        $race = Race::query()->where('id','=', $raceId)->first();

        return $race;
    }
}
