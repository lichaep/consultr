<?php

namespace App\Http\Requests;

class BaseRequest
{
    const ORDER_BY = 'orderBy';
    const ORDER_BY_DIRECTION = 'orderByDirection';

    public function orderBy()
    {
        return request()->query(self::ORDER_BY);
    }

    public function orderByDirection()
    {
        return request()->query(self::ORDER_BY_DIRECTION);
    }
}
