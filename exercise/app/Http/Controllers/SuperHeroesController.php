<?php

namespace App\Http\Controllers;

use App\Http\Requests\SuperHeroesListRequest;
use App\Http\Transformers\SuperHeroTransformer;
use App\Models\SuperHero;

class SuperHeroesController extends Controller
{
    public function list(SuperHeroesListRequest $request, SuperHeroTransformer $transformer)
    {
        $superHeroes = SuperHero::filterList($request);

        return $superHeroes;
    }
}
