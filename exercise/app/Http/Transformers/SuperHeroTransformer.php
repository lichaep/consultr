<?php

namespace App\Http\Transformers;

use App\Models\Publisher;
use App\Models\Race;
use App\Models\SuperHero;

class SuperHeroTransformer
{
    public function transform(SuperHero $superHero)
    {
        return [
            'id' => $superHero->id,
            'name' => $superHero->name,
            'fullName' => $superHero->fullName,
            'strength' => $superHero->strength,
            'speed' => $superHero->speed,
            'durability' => $superHero->durability,
            'power' => $superHero->power,
            'combat' => $superHero->combat,
            'heightInch' => $superHero->heightInch,
            'heightCentimeters' => $superHero->heightCentimeters,
            'weightPounds' => $superHero->weightPounds,
            'weightKilogramos' => $superHero->weightKilogramos,
            'eyeColor' => $superHero->eyeColor,
            'hairColor' => $superHero->hairColor,
            'race' => $this->race($superHero->race),
            'publisher' => $this->publisher($superHero->publisher),
        ];
    }

    private function race(?Race $race): ?array {
        if (!$race) {
            return null;
        }

        return [
            'id' => $race->id,
            'description' => $race->description
        ];
    }

    private function publisher(?Publisher $publisher): ?array {
        if (!$publisher) {
            return null;
        }

        return [
            'id' => $publisher->id,
            'description' => $publisher->description
        ];
    }

    public function map($data)
    {
        return $data->map(function (SuperHero $superHero) {
            return $this->transform($superHero);
        });
    }
}
