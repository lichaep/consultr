<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    /** @var int */
    private $id;

    /** @var string */
    private $description;

    protected $fillable = ['description'];
}
