<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    /** @var int */
    private $id;

    /** @var string */
    private $description;

    protected $fillable = ['description'];
}
