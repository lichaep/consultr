<?php

namespace App\Models;

use App\Http\Requests\SuperHeroesListRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class SuperHero extends Model
{
    /** @var int */
    private $id;

    /** @var Race */
    private $race;

    /** @var Publisher */
    private $publisher;

    /** @var string */
    private $name;

    /** @var string */
    private $fullname;

    /** @var int */
    private $strength;

    /** @var int */
    private $speed;

    /** @var int */
    private $durability;

    /** @var int */
    private $power;

    /** @var int */
    private $combat;

    private static $orderByDirectionDefault = 'DESC';

    protected $fillable = [
        'name', 'fullName', 'strength', 'speed', 'durability', 'power', 'combat', 'race_id', 'heightInch',
        'heightCentimeters', 'weightPounds', 'weightKilogramos', 'eyeColor', 'hairColor', 'publisher_id'
    ];

    public function publisher()
    {
        return $this->belongsTo(Publisher::class, 'publisher_id', 'id');
    }

    public function race()
    {
        return $this->belongsTo(Race::class, 'race_id', 'id');
    }

    public static function filterList(SuperHeroesListRequest $request)
    {
        $query = static::query();

        if ($request->name()) {
            $query->where('name', 'like', '%' . $request->name() . '%');
        }

        if ($request->fullName()) {
            $query->where('fullName', $request->fullName());
        }

        if ($publisher = $request->publisher()) {
            $query->whereHas('publisher', function($q) use($publisher) {
                $q->where('id', $publisher->id);
            });
        }

        if ($race = $request->race()) {
            $query->whereHas('race', function($q) use($race) {
                $q->where('id', $race->id);
            });
        }

        if ($request->orderBy()) {
            $direction = $request->orderByDirection() ?? self::$orderByDirectionDefault;
            $query->orderBy($request->orderBy(), $direction);
        }

        return $query->paginate();
    }
}
