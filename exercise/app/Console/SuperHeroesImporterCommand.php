<?php

namespace App\Console;

use App\Services\SuperHeroService;
use Illuminate\Console\Command;

class SuperHeroesImporterCommand extends Command
{
    protected $signature = 'superheroes:import:csv';

    public function handle(SuperHeroService $service)
    {
        $service->importFromCsv();
    }
}
