<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuperHeroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('super_heroes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('publisher_id')->nullable();
            $table->unsignedBigInteger('race_id')->nullable();
            $table->foreign('publisher_id')->references('id')->on('publishers');
            $table->foreign('race_id')->references('id')->on('races');
            $table->text('name');
            $table->text('fullname');
            $table->integer('strength');
            $table->integer('speed');
            $table->integer('durability');
            $table->integer('power');
            $table->integer('combat');
            $table->string('heightInch')->nullable();
            $table->string('heightCentimeters')->nullable();
            $table->string('weightPounds')->nullable();
            $table->string('weightKilogramos')->nullable();
            $table->string('eyeColor')->nullable();
            $table->string('hairColor')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('super_heroes');
    }
}
